let table = [[18, 19, 20, 21, 22, 23, 24, 25],
	[26, 27, 28, 29, 30, 31, 32, 33],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[10, 11, 12, 13, 14, 15, 16, 17],
	[1, 2, 3, 4, 5, 6, 7, 8]];

let firstMove = {10: 0, 11: 0, 12: 0, 13: 0, 14: 0, 15: 0, 16: 0, 17: 0, 26: 0, 27: 0, 28: 0, 29: 0, 30: 0,
	31: 0, 32: 0, 33: 0, 1: 0, 8: 0, 18: 0, 25: 0, 5: 0, 22: 0};

let chessTypes = {1: 2, 2: 3, 3: 4, 4: 5, 5: 6, 6: 4, 7: 3, 8: 2, 10: 1, 11: 1, 12: 1, 13: 1, 14: 1, 15: 1, 16: 1, 
	17: 1, 18: 2, 19: 3, 20: 4, 21: 5, 22: 6, 23: 4, 24: 3, 25: 2, 26: 1, 27: 1, 28: 1, 29: 1, 30: 1, 31: 1, 32: 1,
	33: 1};

let enPassant = {10: 0, 11: 0, 12: 0, 13: 0, 14: 0, 15: 0, 16: 0, 17: 0, 26: 0, 27: 0, 28: 0, 29: 0, 30: 0, 
	31: 0, 32: 0, 33: 0};

let blackCaptured = [];

let whiteCaptured = [];

let chessesChanger = {
	1: "<i class=\"fas fa-chess-rook yellow\" onclick=\"changeChess(1)\"></i>",
	2: "<i class=\"fas fa-chess-knight yellow\" onclick=\"changeChess(2)\"></i>",
	3: "<i class=\"fas fa-chess-bishop yellow\" onclick=\"changeChess(3)\"></i>",
	4: "<i class=\"fas fa-chess-queen yellow\" onclick=\"changeChess(4)\"></i>",
	5: "<i class=\"fas fa-chess-king yellow\" onclick=\"changeChess(5)\"></i>",
	6: "<i class=\"fas fa-chess-bishop yellow\" onclick=\"changeChess(6)\"></i>",
	7: "<i class=\"fas fa-chess-knight yellow\" onclick=\"changeChess(7)\"></i>",
	8: "<i class=\"fas fa-chess-rook yellow\" onclick=\"changeChess(8)\"></i>",
	10: "<i class=\"fas fa-chess-pawn yellow\" onclick=\"changeChess(10)\"></i>",
	11: "<i class=\"fas fa-chess-pawn yellow\" onclick=\"changeChess(11)\"></i>",
	12: "<i class=\"fas fa-chess-pawn yellow\" onclick=\"changeChess(12)\"></i>",
	13: "<i class=\"fas fa-chess-pawn yellow\" onclick=\"changeChess(13)\"></i>",
	14: "<i class=\"fas fa-chess-pawn yellow\" onclick=\"changeChess(14)\"></i>",
	15: "<i class=\"fas fa-chess-pawn yellow\" onclick=\"changeChess(15)\"></i>",
	16: "<i class=\"fas fa-chess-pawn yellow\" onclick=\"changeChess(16)\"></i>",
	17: "<i class=\"fas fa-chess-pawn yellow\" onclick=\"changeChess(17)\"></i>",

	18: "<i class=\"fas fa-chess-rook blackt\" onclick=\"changeChess(18)\"></i>",
	19: "<i class=\"fas fa-chess-knight blackt\" onclick=\"changeChess(19)\"></i>",
	20: "<i class=\"fas fa-chess-bishop blackt\" onclick=\"changeChess(20)\"></i>",
	21: "<i class=\"fas fa-chess-queen blackt\" onclick=\"changeChess(21)\"></i>",
	22: "<i class=\"fas fa-chess-king blackt\" onclick=\"changeChess(22)\"></i>",
	23: "<i class=\"fas fa-chess-bishop blackt\" onclick=\"changeChess(23)\"></i>",
	24: "<i class=\"fas fa-chess-knight blackt\" onclick=\"changeChess(24)\"></i>",
	25: "<i class=\"fas fa-chess-rook blackt\" onclick=\"changeChess(25)\"></i>",
	26: "<i class=\"fas fa-chess-pawn blackt\" onclick=\"changeChess(26)\"></i>",
	27: "<i class=\"fas fa-chess-pawn blackt\" onclick=\"changeChess(27)\"></i>",
	28: "<i class=\"fas fa-chess-pawn blackt\" onclick=\"changeChess(28)\"></i>",
	29: "<i class=\"fas fa-chess-pawn blackt\" onclick=\"changeChess(29)\"></i>",
	30: "<i class=\"fas fa-chess-pawn blackt\" onclick=\"changeChess(30)\"></i>",
	31: "<i class=\"fas fa-chess-pawn blackt\" onclick=\"changeChess(31)\"></i>",
	32: "<i class=\"fas fa-chess-pawn blackt\" onclick=\"changeChess(32)\"></i>",
	33: "<i class=\"fas fa-chess-pawn blackt\" onclick=\"changeChess(33)\"></i>"
};

let chesses = {
	1: "<i class=\"fas fa-chess-rook yellow\" onclick=\"selectChess(event, 1, 1)\"></i>",
	2: "<i class=\"fas fa-chess-knight yellow\" onclick=\"selectChess(event, 1, 2)\"></i>",
	3: "<i class=\"fas fa-chess-bishop yellow\" onclick=\"selectChess(event, 1, 3)\"></i>",
	4: "<i class=\"fas fa-chess-queen yellow\" onclick=\"selectChess(event, 1, 4)\"></i>",
	5: "<i class=\"fas fa-chess-king yellow\" onclick=\"selectChess(event, 1, 5)\"></i>",
	6: "<i class=\"fas fa-chess-bishop yellow\" onclick=\"selectChess(event, 1, 6)\"></i>",
	7: "<i class=\"fas fa-chess-knight yellow\" onclick=\"selectChess(event, 1, 7)\"></i>",
	8: "<i class=\"fas fa-chess-rook yellow\" onclick=\"selectChess(event, 1, 8)\"></i>",
	9: "",
	10: "<i class=\"fas fa-chess-pawn yellow\" onclick=\"selectChess(event, 1, 10)\"></i>",
	11: "<i class=\"fas fa-chess-pawn yellow\" onclick=\"selectChess(event, 1, 11)\"></i>",
	12: "<i class=\"fas fa-chess-pawn yellow\" onclick=\"selectChess(event, 1, 12)\"></i>",
	13: "<i class=\"fas fa-chess-pawn yellow\" onclick=\"selectChess(event, 1, 13)\"></i>",
	14: "<i class=\"fas fa-chess-pawn yellow\" onclick=\"selectChess(event, 1, 14)\"></i>",
	15: "<i class=\"fas fa-chess-pawn yellow\" onclick=\"selectChess(event, 1, 15)\"></i>",
	16: "<i class=\"fas fa-chess-pawn yellow\" onclick=\"selectChess(event, 1, 16)\"></i>",
	17: "<i class=\"fas fa-chess-pawn yellow\" onclick=\"selectChess(event, 1, 17)\"></i>",

	18: "<i class=\"fas fa-chess-rook blackt\" onclick=\"selectChess(event, 2, 18)\"></i>",
	19: "<i class=\"fas fa-chess-knight blackt\" onclick=\"selectChess(event, 2, 19)\"></i>",
	20: "<i class=\"fas fa-chess-bishop blackt\" onclick=\"selectChess(event, 2, 20)\"></i>",
	21: "<i class=\"fas fa-chess-queen blackt\" onclick=\"selectChess(event, 2, 21)\"></i>",
	22: "<i class=\"fas fa-chess-king blackt\" onclick=\"selectChess(event, 2, 22)\"></i>",
	23: "<i class=\"fas fa-chess-bishop blackt\" onclick=\"selectChess(event, 2, 23)\"></i>",
	24: "<i class=\"fas fa-chess-knight blackt\" onclick=\"selectChess(event, 2, 24)\"></i>",
	25: "<i class=\"fas fa-chess-rook blackt\" onclick=\"selectChess(event, 2, 25)\"></i>",
	26: "<i class=\"fas fa-chess-pawn blackt\" onclick=\"selectChess(event, 2, 26)\"></i>",
	27: "<i class=\"fas fa-chess-pawn blackt\" onclick=\"selectChess(event, 2, 27)\"></i>",
	28: "<i class=\"fas fa-chess-pawn blackt\" onclick=\"selectChess(event, 2, 28)\"></i>",
	29: "<i class=\"fas fa-chess-pawn blackt\" onclick=\"selectChess(event, 2, 29)\"></i>",
	30: "<i class=\"fas fa-chess-pawn blackt\" onclick=\"selectChess(event, 2, 30)\"></i>",
	31: "<i class=\"fas fa-chess-pawn blackt\" onclick=\"selectChess(event, 2, 31)\"></i>",
	32: "<i class=\"fas fa-chess-pawn blackt\" onclick=\"selectChess(event, 2, 32)\"></i>",
	33: "<i class=\"fas fa-chess-pawn blackt\" onclick=\"selectChess(event, 2, 33)\"></i>"
};

let x;
let y;

let cp = 0;
let ur = 0;

let oor = 0;

let chch = 9;

let cid = "";

let move = 1;

function drawChess() {
	for (let i = 0; i < table.length; i++) {
		for (let j = 0; j < table.length; j++) {
			if (table[i][j] !== 0) {
				cid = "c" + i + j;
				document.getElementById(cid).innerHTML = chesses[table[i][j]];
				document.getElementById(cid).onclick = function() {moveChess(i, j)};
			} else {
				cid = "c" + i + j;
				document.getElementById(cid).innerHTML = "";
				document.getElementById(cid).onclick = function() {moveChess(i, j)};
			}
		}
	}

	document.getElementById("whiteCaptured").innerHTML = "";

	for (let i = 0; i < whiteCaptured.length; i++) {
		document.getElementById("whiteCaptured").innerHTML += chesses[whiteCaptured[i]];
	}

	document.getElementById("blackCaptured").innerHTML = "";

	for (let i = 0; i < blackCaptured.length; i++) {
		document.getElementById("blackCaptured").innerHTML += chesses[blackCaptured[i]];
	}
}

function resetChess() {
	table = [[18, 19, 20, 21, 22, 23, 24, 25],
	[26, 27, 28, 29, 30, 31, 32, 33],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0, 0, 0, 0],
	[10, 11, 12, 13, 14, 15, 16, 17],
	[1, 2, 3, 4, 5, 6, 7, 8]];

	firstMove = {10: 0, 11: 0, 12: 0, 13: 0, 14: 0, 15: 0, 16: 0, 17: 0, 26: 0, 27: 0, 28: 0, 29: 0, 30: 0,
	31: 0, 32: 0, 33: 0, 1: 0, 8: 0, 18: 0, 25: 0, 5: 0, 22: 0};

	enPassant = {10: 0, 11: 0, 12: 0, 13: 0, 14: 0, 15: 0, 16: 0, 17: 0, 26: 0, 27: 0, 28: 0, 29: 0, 30: 0, 31: 0, 
		32: 0, 33: 0};

	move = 1;

	chch = 9;

	cp = 0;

	oor = 0;

	blackCaptured = [];

	whiteCaptured = [];

	drawChess();
}

function selectChess(event, selectedC, chessID) {
	if (selectedC === move) {
		let sC = document.getElementsByClassName("fas");

		for (let i = 0; i < sC.length; i++) {
			sC[i].className = sC[i].className.replace(" selected", "");
		}

		event.currentTarget.className += " selected";

		for (let i = 0; i < table.length; i++) {
			for (let j = 0; j < table[i].length; j++) {
				if (table[i][j] == chessID) {
					x = i;
					y = j;
				}
			}
		}
	}
}

function moveChess(ax, bx) {
	let lC = document.getElementsByClassName("selected");
	let chessColors;
	let chessColor;
	let captureColor;
	let choosed;
	let chessType = chessTypes[table[x][y]];

	table[x][y] > 17 ? chessColor = 2 : chessColor = 1;
	table[ax][bx] > 17 ? captureColor = 2 : captureColor = 1;
	chessColor === 1 ? chessColors = 2 : chessColors = 1;

	if (table[ax][bx] === 0) {
		if (lC.length !== undefined && lC.length === 1 && checkPos(x, y, ax, bx)) {
			if (chessType === 1) {
				if (chessColor === 1 && ax === 0) {
					if (!checkChack(x, y, ax, bx, chessColor)) {
						choosed = chooseChess(chessColor);
						table[ax][bx] = chch;
						table[x][y] = 0;
						if (table[ax][bx]) {
							drawChess();
							if (checkMate(chessColors)) {
								if (!checkCMate(chessColors)) {
									alert("Sachas");
								} else {
									alert("Sachas ir matas!!!");
								}
							}

							move === 1 ? move = 2 : move = 1;
						}
					}
				} else if (chessColor === 2 && ax === 7) {
					if (!checkChack(x, y, ax, bx, chessColor)) {
						choosed = chooseChess(chessColor);
						table[ax][bx] = chch;
						table[x][y] = 0;
						if (table[ax][bx]) {
							drawChess();
							if (checkMate(chessColors)) {
								if (!checkCMate(chessColors)) {
									alert("Sachas");
								} else {
									alert("Sachas ir matas!!!");
								}
							}

							move === 1 ? move = 2 : move = 1;
						}
					}
				} else {
					if (!checkChack(x, y, ax, bx, chessColor)) {
						firstMove[table[x][y]] = 1;
						table[ax][bx] = table[x][y];
						table[x][y] = 0;
						drawChess();
						if (checkMate(chessColors)) {
							if (!checkCMate(chessColors)) {
								alert("Sachas");
							} else {
								alert("Sachas ir matas!!!");
							}
						}

						move === 1 ? move = 2 : move = 1;
					}
				}
			} else {
				if (!checkChack(x, y, ax, bx, chessColor)) {
					if (table[x][y] === 5 || table[x][y] === 22 || chessTypes[table[x][y]] === 2) {
						firstMove[table[x][y]] = 1;
					}
					table[ax][bx] = table[x][y];
					table[x][y] = 0;
					drawChess();
					if (checkMate(chessColors)) {
						if (!checkCMate(chessColors)) {
							alert("Sachas");
						} else {
							alert("Sachas ir matas!!!");
						}
					}

					move === 1 ? move = 2 : move = 1;
				}
			}
		}
	} else {
		if (chessColor !== captureColor && checkPos(x, y, ax, bx)) {
			if (chessType === 1 && enPassant[table[ax][bx]] === 1 && chessTypes[table[ax][bx]] === 1 && x === ax) {
				if (!checkChack(x, y, ax, bx, chessColor)) {
					if (chessColor === 1) {
						table[ax-1][bx] = table[x][y];
					}

					if (chessColor === 2) {
						table[ax+1][bx] = table[x][y];
					}
					chessColor === 1 ? blackCaptured.push(table[ax][bx]) : whiteCaptured.push(table[ax][bx]);
					table[ax][bx] = 0;
					table[x][y] = 0;
					enPassant[table[ax][bx]] = 0;
					drawChess();
					if (checkMate(chessColors)) {
						if (!checkCMate(chessColors)) {
							alert("Sachas");
						} else {
							alert("Sachas ir matas!!!");
						}
					}

					move === 1 ? move = 2 : move = 1;
				}
			} else if (chessType === 1 && ax === 0) {
				if (!checkChack(x, y, ax, bx, chessColor)) {
					chessColor === 1 ? blackCaptured.push(table[ax][bx]) : whiteCaptured.push(table[ax][bx]);
					choosed = chooseChess(chessColor);
					table[ax][bx] = chch;
					table[x][y] = 0;
					if (table[ax][bx]) {
						drawChess();
						if (checkMate(chessColors)) {
							if (!checkCMate(chessColors)) {
								alert("Sachas");
							} else {
								alert("Sachas ir matas!!!");
							}
						}

						move === 1 ? move = 2 : move = 1;
					}
				}
			} else if (chessType === 1 && ax === 7) {
				if (!checkChack(x, y, ax, bx, chessColor)) {
					chessColor === 1 ? blackCaptured.push(table[ax][bx]) : whiteCaptured.push(table[ax][bx]);
					choosed = chooseChess(chessColor);
					table[ax][bx] = chch;
					table[x][y] = 0;
					if (table[ax][bx]) {
						drawChess();
						if (checkMate(chessColors)) {
							if (!checkCMate(chessColors)) {
								alert("Sachas");
							} else {
								alert("Sachas ir matas!!!");
							}
						}

						move === 1 ? move = 2 : move = 1;
					}
				}
			} else {
				if (!checkChack(x, y, ax, bx, chessColor)) {
					if (table[x][y] === 5 || table[x][y] === 22 || chessTypes[table[x][y]] === 2) {
						firstMove[table[x][y]] = 1;
					}
					chessColor === 1 ? blackCaptured.push(table[ax][bx]) : whiteCaptured.push(table[ax][bx]);
					table[ax][bx] = table[x][y];
					table[x][y] = 0;
					drawChess();
					if (checkMate(chessColors)) {
						if (!checkCMate(chessColors)) {
							alert("Sachas");
						} else {
							alert("Sachas ir matas!!!");
						}
					}

					move === 1 ? move = 2 : move = 1;
				}
			}
		}
	}
}

function chooseChess(col) {
	let captureC = document.getElementById("selectChess");

	captureC.style.display = "grid";
	captureC.innerHTML = "";

	if (col === 1) {
		for (let i = 0; i < whiteCaptured.length; i++) {
			captureC.innerHTML += chessesChanger[whiteCaptured[i]];
		}
	}

	if (col === 2) {
		for (let i = 0; i < blackCaptured.length; i++) {
			captureC.innerHTML += chessesChanger[blackCaptured[i]];
		}
	}
}

function changeChess(ccid) {
	chch = ccid;
	for (let i = 0; i < table.length; i++) {
		for (let j = 0; j < table[i].length; j++) {
			if (table[i][j] === 9) {
				table[i][j] = ccid;
				chch = 9;
			}
		}
	}
	drawChess();

	let captureC = document.getElementById("selectChess");
	captureC.style.display = "none";
}

function checkChack (ox, oy, dx, dy, mc) {
	let currp;
	let currc;
	let ccp;
	let urr;

	oor = 1;

	ccp = cp;
	urr = ur;
	currc = table[dx][dy];
	table[dx][dy] = table[ox][oy];
	table[ox][oy] = 0;
	currp = checkMate(mc);
	table[ox][oy] = table[dx][dy];
	table[dx][dy] = currc;
	cp = ccp;
	ur = urr;

	oor = 0;

	return currp;
}

function checkCMate(mc) {
	let mx;
	let my;
	let gx;
	let gy;
	let fx;
	let fy;
	let cmp;
	let rf = [];

	if (mc === 1) {
		for (let i = 0; i < table.length; i++) {
			for (let j = 0; j < table[i].length; j++) {
				if (table[i][j] === 5) {
					mx = i;
					my = j;
				}

				if (table[i][j] !== 0 && table[i][j] < 18 && table[i][j] !== 5) {
					rf.push(table[i][j]);
				}
			}
		}
	}

	if (mc === 2) {
		for (let i = 0; i < table.length; i++) {
			for (let j = 0; j < table[i].length; j++) {
				if (table[i][j] === 22) {
					mx = i;
					my = j;
				}

				if (table[i][j] !== 0 && table[i][j] > 17 && table[i][j] !== 22) {
					rf.push(table[i][j]);
				}
			}
		}
	}

	if (checkMate(mc)) {
		cmp = 0;

		oor = 1;

		if (mx === 7) {
			if (my === 0) {
				if (checkPos(mx, my, mx - 1, my)) {
					if (!checkChack(mx, my, mx - 1, my, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx - 1, my + 1)) {
					if (!checkChack(mx, my, mx - 1, my + 1, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx, my + 1)) {
					if (!checkChack(mx, my, mx, my + 1, mc)) {
						cmp++;
					}
				}
			} else if (my === 7) {
				if (checkPos(mx, my, mx - 1, my)) {
					if (!checkChack(mx, my, mx - 1, my, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx - 1, my - 1)) {
					if (!checkChack(mx, my, mx - 1, my - 1, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx, my - 1)) {
					if (!checkChack(mx, my, mx, my - 1, mc)) {
						cmp++;
					}
				}
			} else {
				if (checkPos(mx, my, mx - 1, my)) {
					if (!checkChack(mx, my, mx - 1, my, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx - 1, my - 1)) {
					if (!checkChack(mx, my, mx - 1, my - 1, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx, my - 1)) {
					if (!checkChack(mx, my, mx, my - 1, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx - 1, my + 1)) {
					if (!checkChack(mx, my, mx - 1, my + 1, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx, my + 1)) {
					if (!checkChack(mx, my, mx, my + 1, mc)) {
						cmp++;
					}
				}
			}
		} else if (mx === 0) {
			if (my === 0) {
				if (checkPos(mx, my, mx + 1, my)) {
					if (!checkChack(mx, my, mx + 1, my, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx + 1, my + 1)) {
					if (!checkChack(mx, my, mx + 1, my + 1, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx, my + 1)) {
					if (!checkChack(mx, my, mx, my + 1, mc)) {
						cmp++;
					}
				}
			} else if (my === 7) {
				if (checkPos(mx, my, mx + 1, my)) {
					if (!checkChack(mx, my, mx + 1, my, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx + 1, my - 1)) {
					if (!checkChack(mx, my, mx + 1, my - 1, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx, my - 1)) {
					if (!checkChack(mx, my, mx, my - 1, mc)) {
						cmp++;
					}
				}
			} else {
				if (checkPos(mx, my, mx + 1, my)) {
					if (!checkChack(mx, my, mx + 1, my, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx + 1, my - 1)) {
					if (!checkChack(mx, my, mx + 1, my - 1, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx, my - 1)) {
					if (!checkChack(mx, my, mx, my - 1, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx + 1, my + 1)) {
					if (!checkChack(mx, my, mx + 1, my + 1, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx, my + 1)) {
					if (!checkChack(mx, my, mx, my + 1, mc)) {
						cmp++;
					}
				}
			}
		} else {
			if (my === 0) {
				if (checkPos(mx, my, mx + 1, my)) {
					if (!checkChack(mx, my, mx + 1, my, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx + 1, my + 1)) {
					if (!checkChack(mx, my, mx + 1, my + 1, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx, my + 1)) {
					if (!checkChack(mx, my, mx, my + 1, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx - 1, my + 1)) {
					if (!checkChack(mx, my, mx - 1, my + 1, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx - 1, my)) {
					if (!checkChack(mx, my, mx - 1, my, mc)) {
						cmp++;
					}
				}
			} else if (my === 7) {
				if (checkPos(mx, my, mx + 1, my)) {
					if (!checkChack(mx, my, mx + 1, my, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx + 1, my - 1)) {
					if (!checkChack(mx, my, mx + 1, my - 1, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx, my - 1)) {
					if (!checkChack(mx, my, mx, my - 1, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx - 1, my - 1)) {
					if (!checkChack(mx, my, mx - 1, my - 1, mc)) {
						cmp++;
					}
				}
				if (checkPos(mx, my, mx - 1, my)) {
					if (!checkChack(mx, my, mx - 1, my, mc)) {
						cmp++;
					}
				}
			} else {
				if (checkPos(mx, my, mx - 1, my)) {
					if (!checkChack(mx, my, mx - 1, my, mc)) {
						cmp++;
					}
				}

				if (checkPos(mx, my, mx - 1, my + 1)) {
					if (!checkChack(mx, my, mx - 1, my + 1, mc)) {
						cmp++;
					}
				}

				if (checkPos(mx, my, mx, my + 1)) {
					if (!checkChack(mx, my, mx, my + 1, mc)) {
						cmp++;
					}
				}

				if (checkPos(mx, my, mx + 1, my + 1)) {
					if (!checkChack(mx, my, mx + 1, my + 1, mc)) {
						cmp++;
					}
				}

				if (checkPos(mx, my, mx + 1, my)) {
					if (!checkChack(mx, my, mx + 1, my, mc)) {
						cmp++;
					}
				}

				if (checkPos(mx, my, mx + 1, my - 1)) {
					if (!checkChack(mx, my, mx + 1, my - 1, mc)) {
						cmp++;
					}
				}

				if (checkPos(mx, my, mx, my - 1)) {
					if (!checkChack(mx, my, mx, my - 1, mc)) {
						cmp++;
					}
				}

				if (checkPos(mx, my, mx - 1, my - 1)) {
					if (!checkChack(mx, my, mx - 1, my - 1, mc)) {
						cmp++;
					}
				}
			}
		}

		oor = 0;

		if (cp === 1) {
			fx = 0;

			for (let i = mx - 1; i >= 0; i--) {
				if (table[i][my] === 0) {
					fx++;
				} else {
					break;
				}
			}

			for (let i = 0; i < rf.length; i++) {
				for (let j = 0; j < table.length; j++) {
					for (let k = 0; k < table[j].length; k++) {
						if (table[j][k] === rf[i]) {
							gx = j;
							gy = k;
						}
					}
				}

				for (let i = 1; i <= fx + 1; i++) {
					if (checkPos(gx, gy, mx - i, my)) {
						if (!checkChack(gx, gy, mx - i, my, mc)) {
							cmp++;
						}
					}
				}
			}
		}

		if (cp === 2) {
			let ni = 0;
			fx = 0;

			for (let i = mx - 1; i >= 0; i--) {
				ni++;

				if (table[i][my + ni] === 0) {
					fx++;
				} else {
					break;
				}
			}

			for (let i = 0; i < rf.length; i++) {
				for (let j = 0; j < table.length; j++) {
					for (let k = 0; k < table[j].length; k++) {
						if (table[j][k] === rf[i]) {
							gx = j;
							gy = k;
						}
					}
				}

				for (let i = 1; i <= fx + 1; i++) {
					if (checkPos(gx, gy, mx - i, my + i)) {
						if (!checkChack(gx, gy, mx - i, my + i, mc)) {
							cmp++;
						}
					}
				}
			}
		}

		if (cp === 3) {
			fx = 0;

			for (let i = my + 1; i <= 7; i++) {
				if (table[mx][i] === 0) {
					fx++;
				} else {
					break;
				}
			}

			for (let i = 0; i < rf.length; i++) {
				for (let j = 0; j < table.length; j++) {
					for (let k = 0; k < table[j].length; k++) {
						if (table[j][k] === rf[i]) {
							gx = j;
							gy = k;
						}
					}
				}

				for (let i = 1; i <= fx + 1; i++) {
					if (checkPos(gx, gy, mx, my + i)) {
						if (!checkChack(gx, gy, mx, my + i, mc)) {
							cmp++;
						}
					}
				}
			}
		}

		if (cp === 4) {
			let ni = 0;
			fx = 0;

			for (let i = mx + 1; i <= 7; i++) {
				ni++;

				if (table[i][my + ni] === 0) {
					fx++;
				} else {
					break;
				}
			}

			for (let i = 0; i < rf.length; i++) {
				for (let j = 0; j < table.length; j++) {
					for (let k = 0; k < table[j].length; k++) {
						if (table[j][k] === rf[i]) {
							gx = j;
							gy = k;
						}
					}
				}

				for (let i = 1; i <= fx + 1; i++) {
					if (checkPos(gx, gy, mx + i, my + i)) {
						if (!checkChack(gx, gy, mx + i, my + i, mc)) {
							cmp++;
						}
					}
				}
			}
		}

		if (cp === 5) {
			fx = 0;

			for (let i = mx + 1; i <= 7; i++) {
				if (table[i][my] === 0) {
					fx++;
				} else {
					break;
				}
			}

			for (let i = 0; i < rf.length; i++) {
				for (let j = 0; j < table.length; j++) {
					for (let k = 0; k < table[j].length; k++) {
						if (table[j][k] === rf[i]) {
							gx = j;
							gy = k;
						}
					}
				}

				for (let i = 1; i <= fx + 1; i++) {
					if (checkPos(gx, gy, mx + i, my)) {
						if (!checkChack(gx, gy, mx + i, my, mc)) {
							cmp++;
						}
					}
				}
			}
		}

		if (cp === 6) {
			let ni = 0;
			fx = 0;

			for (let i = mx + 1; i <= 7; i++) {
				ni++;

				if (table[i][my - ni] === 0) {
					fx++;
				} else {
					break;
				}
			}

			for (let i = 0; i < rf.length; i++) {
				for (let j = 0; j < table.length; j++) {
					for (let k = 0; k < table[j].length; k++) {
						if (table[j][k] === rf[i]) {
							gx = j;
							gy = k;
						}
					}
				}

				for (let i = 1; i <= fx + 1; i++) {
					if (checkPos(gx, gy, mx + i, my - i)) {
						if (!checkChack(gx, gy, mx + i, my - i, mc)) {
							cmp++;
						}
					}
				}
			}
		}

		if (cp === 7) {
			fx = 0;

			for (let i = my - 1; i >= 0; i--) {
				if (table[mx][i] === 0) {
					fx++;
				} else {
					break;
				}
			}

			for (let i = 0; i < rf.length; i++) {
				for (let j = 0; j < table.length; j++) {
					for (let k = 0; k < table[j].length; k++) {
						if (table[j][k] === rf[i]) {
							gx = j;
							gy = k;
						}
					}
				}

				for (let i = 1; i <= fx + 1; i++) {
					if (checkPos(gx, gy, mx, my - i)) {
						if (!checkChack(gx, gy, mx, my - i, mc)) {
							cmp++;
						}
					}
				}
			}
		}

		if (cp === 8) {
			let ni = 0;
			fx = 0;

			for (let i = mx - 1; i >= 0; i--) {
				ni++;

				if (table[i][my - ni] === 0) {
					fx++;
				} else {
					break;
				}
			}

			for (let i = 0; i < rf.length; i++) {
				for (let j = 0; j < table.length; j++) {
					for (let k = 0; k < table[j].length; k++) {
						if (table[j][k] === rf[i]) {
							gx = j;
							gy = k;
						}
					}
				}

				for (let i = 1; i <= fx + 1; i++) {
					if (checkPos(gx, gy, mx - i, my - i)) {
						if (!checkChack(gx, gy, mx - i, my - i, mc)) {
							cmp++;
						}
					}
				}
			}
		}

		if (cp = 9) {
			let posx;
			let posy;

			if (ur === 1) {
				posx = mx - 2;
				posy = my + 1;
			}
			if (ur === 2) {
				posx = mx - 1;
				posy = my + 2;
			}
			if (ur === 3) {
				posx = mx + 1;
				posy = my + 2;
			}
			if (ur === 4) {
				posx = mx + 2;
				posy = my + 1;
			}
			if (ur === 5) {
				posx = mx + 2;
				posy = my - 1;
			}
			if (ur === 6) {
				posx = mx + 1;
				posy = my - 2;
			}
			if (ur === 7) {
				posx = mx - 1;
				posy = my - 2;
			}
			if (ur === 8) {
				posx = mx - 2;
				posy = my - 1;
			}

			for (let i = 0; i < rf.length; i++) {
				for (let j = 0; j < table.length; j++) {
					for (let k = 0; k < table[j].length; k++) {
						if (table[j][k] === rf[i]) {
							gx = j;
							gy = k;
						}
					}
				}
				if (checkPos(gx, gy, posx, posy)) {
					if (!checkChack(gx, gy, posx, posy, mc)) {
						cmp++;
					}
				}
			}
		}
	}

	if (cmp === 0) {
		return true;
	} else {
		return false;
	}
}

function checkMate(mc) {
	let wx;
	let wy;
	let lx;
	let ly;
	let ptx;
	let pbx;
	let prx;
	let plx;
	let cormt;
	let cormb;
	let cormr;
	let corml;
	let cormtr;
	let cormtl;
	let cormbr;
	let cormbl;
	let cormp;
	let cormz;

	for (let i = 0; i < table.length; i++) {
		for (let j = 0; j < table[i].length; j++) {
			if (table[i][j] === 5) {
				wx = i;
				wy = j;
			}

			if (table[i][j] === 22) {
				lx = i;
				ly = j;
			}
		}
	}

	if (mc === 1) {
		for (let i = wx - 1; i >= 0; i--) {  // I virsu
			if (table[i][wy] < 18 && table[i][wy] !== 0) {
				cormt = false;
				break;
			} else {
				if (chessTypes[table[i][wy]] === 2 || chessTypes[table[i][wy]] === 5) {
					cormt = true;
					cp = 1;
					break;
				} else if (table[i][wy] !== 0) {
					cormt = false;
					break;
				}
			}
		}

		for (let i = wx + 1; i <= 7; i++) {  // I apacia
			if (table[i][wy] < 18 && table[i][wy] !== 0) {
				cormb = false;
				break;
			} else {
				if (chessTypes[table[i][wy]] === 2 || chessTypes[table[i][wy]] === 5) {
					cormb = true;
					cp = 5;
					break;
				} else if (table[i][wy] !== 0) {
					cormb = false;
					break;
				}
			}
		}

		for (let i = wy + 1; i <= 7; i++) {  // I desine
			if (table[wx][i] < 18 && table[wx][i] !== 0) {
				cormr = false;
				break;
			} else {
				if (chessTypes[table[wx][i]] === 2 || chessTypes[table[wx][i]] === 5) {
					cormr = true;
					cp = 3;
					break;
				} else if (table[wx][i] !== 0) {
					cormr = false;
					break;
				}
			}
		}

		for (let i = wy - 1; i >= 0; i--) {  // I kaire
			if (table[wx][i] < 18 && table[wx][i] !== 0) {
				corml = false;
				break;
			} else {
				if (chessTypes[table[wx][i]] === 2 || chessTypes[table[wx][i]] === 5) {
					corml = true;
					cp = 7;
					break;
				} else if (table[wx][i] !== 0) {
					corml = false;
					break;
				}
			}
		}

		((7 - wx) - wy) < 0 ? ptx = (7 - wy) : ptx = wx;

		wy - wx < 1 ? pbx = wy : pbx = wx;

		wx - wy > 0 ? prx = 7 - wx : prx = 7 - wy;

		wy - wx > 0 ? plx = wy : plx = 7 - wx;

		for (let i = 1; i <= ptx; i++) { // I virsu desiniau
			if (table[wx - i][wy + i] < 18 && table[wx - i][wy + i] !== 0) {
				cormtr = false;
				break;
			} else {
				if (chessTypes[table[wx - i][wy + i]] === 5 || chessTypes[table[wx - i][wy + i]] === 4) {
					cormtr = true;
					cp = 2;
					break;
				} else if (table[wx - i][wy + i] !== 0) {
					cormtr = false;
					break;
				}
			}
		}

		for (let i = 1; i <= pbx; i++) { // I virsu kairiau
			if (table[wx - i][wy - i] < 18 && table[wx - i][wy - i] !== 0) {
				cormtl = false;
				break;
			} else {
				if (chessTypes[table[wx - i][wy - i]] === 5 || chessTypes[table[wx - i][wy - i]] === 4) {
					cormtl = true;
					cp = 8;
					break;
				} else if (table[wx - i][wy - i] !== 0) {
					cormtl = false;
					break;
				}
			}
		}

		for (let i = 1; i <= prx; i++) { // I apacia desiniau
			if (table[wx + i][wy + i] < 18 && table[wx + i][wy + i] !== 0) {
				cormbr = false;
				break;
			} else {
				if (chessTypes[table[wx + i][wy + i]] === 5 || chessTypes[table[wx + i][wy + i]] === 4) {
					cormbr = true;
					cp = 4;
					break;
				} else if (table[wx + i][wy + i] !== 0) {
					cormbr = false;
					break;
				}
			}
		}

		for (let i = 1; i <= plx; i++) { // I apacia kairiau
			if (table[wx + i][wy - i] < 18 && table[wx + i][wy - i] !== 0) {
				cormbl = false;
				break;
			} else {
				if (chessTypes[table[wx + i][wy - i]] === 5 || chessTypes[table[wx + i][wy - i]] === 4) {
					cormbl = true;
					cp = 6;
					break;
				} else if (table[wx + i][wy - i] !== 0) {
					cormbl = false;
					break;
				}
			}
		}

		if (wx !== 0) {  // Pestininku
			if (wy === 0) {
				if (chessTypes[table[wx - 1][wy + 1]] === 1 && table[wx - 1][wy + 1] > 18) {
					cormp = true;
				} else {
					cormp = false;
				}
			} else if (wy === 7) {
				if (chessTypes[table[wx - 1][wy - 1]] === 1 && table[wx - 1][wy - 1] > 18) {
					cormp = true;
				} else {
					cormp = false;
				}
			} else {
				if (chessTypes[table[wx - 1][wy + 1]] === 1 && table[wx - 1][wy + 1] > 18) {
					cormp = true;
				} else if (chessTypes[table[wx - 1][wy - 1]] === 1 && table[wx - 1][wy - 1] > 18) {
					cormp = true;
				} else {
					cormp = false;
				}
			}
		}

		if (wx === 7) {  // Zirgu
			if (wy === 0) {
				if (chessTypes[table[wx - 2][wy + 1]] === 3 && table[wx - 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else if (chessTypes[table[wx - 1][wy + 2]] === 3 && table[wx - 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else {
					cormz = false;
				}
			} else if (wy === 1) {
				if (chessTypes[table[wx - 2][wy + 1]] === 3 && table[wx - 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else if (chessTypes[table[wx - 1][wy + 2]] === 3 && table[wx - 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else if (chessTypes[table[wx - 2][wy - 1]] === 3 && table[wx - 2][wy - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else {
					cormz = false;
				}
			} else if (wy === 7) {
				if (chessTypes[table[wx - 2][wy - 1]] === 3 && table[wx - 2][wy - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else if (chessTypes[table[wx - 1][wy - 2]] === 3 && table[wx - 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else {
					cormz = false;
				}
			} else if (wy === 6) {
				if (chessTypes[table[wx - 2][wy - 1]] === 3 && table[wx - 2][wy - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else if (chessTypes[table[wx - 1][wy - 2]] === 3 && table[wx - 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else if (chessTypes[table[wx - 2][wy + 1]] === 3 && table[wx - 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else {
					cormz = false;
				}
			} else {
				if (chessTypes[table[wx - 2][wy + 1]] === 3 && table[wx - 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else if (chessTypes[table[wx - 2][wy - 1]] === 3 && table[wx - 2][wy - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else if (chessTypes[table[wx - 1][wy - 2]] === 3 && table[wx - 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else if (chessTypes[table[wx - 1][wy + 2]] === 3 && table[wx - 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else {
					cormz = false;
				}
			}
		} else if (wx === 6) {
			if (wy === 0) {
				if (chessTypes[table[wx - 2][wy + 1]] === 3 && table[wx - 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else if (chessTypes[table[wx - 1][wy + 2]] === 3 && table[wx - 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else if (chessTypes[table[wx + 1][wy + 2]] === 3 && table[wx + 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else {
					cormz = false;
				}
			} else if (wy === 1) {
				if (chessTypes[table[wx - 2][wy + 1]] === 3 && table[wx - 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else if (chessTypes[table[wx - 1][wy + 2]] === 3 && table[wx - 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else if (chessTypes[table[wx - 2][wy - 1]] === 3 && table[wx - 2][wy - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else if (chessTypes[table[wx + 1][wy + 2]] === 3 && table[wx + 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else {
					cormz = false;
				}
			} else if (wy === 7) {
				if (chessTypes[table[wx - 2][wy - 1]] === 3 && table[wx - 2][wy - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else if (chessTypes[table[wx - 1][wy - 2]] === 3 && table[wx - 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else if (chessTypes[table[wx + 1][wy - 2]] === 3 && table[wx + 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else {
					cormz = false;
				}
			} else if (wy === 6) {
				if (chessTypes[table[wx - 2][wy - 1]] === 3 && table[wx - 2][wy - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else if (chessTypes[table[wx - 1][wy - 2]] === 3 && table[wx - 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else if (chessTypes[table[wx - 2][wy + 1]] === 3 && table[wx - 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else if (chessTypes[table[wx + 1][wy - 2]] === 3 && table[wx + 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else {
					cormz = false;
				}
			} else {
				if (chessTypes[table[wx - 2][wy + 1]] === 3 && table[wx - 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else if (chessTypes[table[wx - 2][wy - 1]] === 3 && table[wx - 2][wy - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else if (chessTypes[table[wx - 1][wy - 2]] === 3 && table[wx - 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else if (chessTypes[table[wx - 1][wy + 2]] === 3 && table[wx - 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else if (chessTypes[table[wx + 1][wy + 2]] === 3 && table[wx + 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else if (chessTypes[table[wx + 1][wy - 2]] === 3 && table[wx + 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else {
					cormz = false;
				}
			}
		} else if (wx === 0) {
			if (wy === 0) {
				if (chessTypes[table[wx + 2][wy + 1]] === 3 && table[wx + 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[wx + 1][wy + 2]] === 3 && table[wx + 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else {
					cormz = false;
				}
			} else if (wy === 1) {
				if (chessTypes[table[wx + 2][wy + 1]] === 3 && table[wx + 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[wx + 1][wy + 2]] === 3 && table[wx + 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else if (chessTypes[table[wx + 2][wy - 1]] === 3 && table[wx + 2][wy - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else {
					cormz = false;
				}
			} else if (wy === 7) {
				if (chessTypes[table[wx + 2][wy - 1]] === 3 && table[wx + 2][wy - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[wx + 1][wy - 2]] === 3 && table[wx + 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else {
					cormz = false;
				}
			} else if (wy === 6) {
				if (chessTypes[table[wx + 2][wy - 1]] === 3 && table[wx + 2][wy - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[wx + 1][wy - 2]] === 3 && table[wx + 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else if (chessTypes[table[wx + 2][wy + 1]] === 3 && table[wx + 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else {
					cormz = false;
				}
			} else {
				if (chessTypes[table[wx + 2][wy + 1]] === 3 && table[wx + 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[wx + 2][wy - 1]] === 3 && table[wx + 2][wy - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[wx + 1][wy + 2]] === 3 && table[wx + 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else if (chessTypes[table[wx + 1][wy - 2]] === 3 && table[wx + 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else {
					cormz = false;
				}
			}
		} else if (wx === 1) {
			if (wy === 0) {
				if (chessTypes[table[wx + 2][wy + 1]] === 3 && table[wx + 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[wx + 1][wy + 2]] === 3 && table[wx + 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else if (chessTypes[table[wx - 1][wy + 2]] === 3 && table[wx - 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else {
					cormz = false;
				}
			} else if (wy === 1) {
				if (chessTypes[table[wx + 2][wy + 1]] === 3 && table[wx + 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[wx + 1][wy + 2]] === 3 && table[wx + 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 3
				} else if (chessTypes[table[wx + 2][wy - 1]] === 3 && table[wx + 2][wy - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[wx - 1][wy + 2]] === 3 && table[wx - 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else {
					cormz = false;
				}
			} else if (wy === 7) {
				if (chessTypes[table[wx + 2][wy - 1]] === 3 && table[wx + 2][wy - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[wx + 1][wy - 2]] === 3 && table[wx + 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else if (chessTypes[table[wx - 1][wy - 2]] === 3 && table[wx - 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else {
					cormz = false;
				}
			} else if (wy === 6) {
				if (chessTypes[table[wx + 2][wy - 1]] === 3 && table[wx + 2][wy - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[wx + 1][wy - 2]] === 3 && table[wx + 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else if (chessTypes[table[wx + 2][wy + 1]] === 3 && table[wx + 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[wx - 1][wy - 2]] === 3 && table[wx - 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else {
					cormz = false;
				}
			} else {
				if (chessTypes[table[wx + 2][wy + 1]] === 3 && table[wx + 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[wx + 2][wy - 1]] === 3 && table[wx + 2][wy - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[wx + 1][wy + 2]] === 3 && table[wx + 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else if (chessTypes[table[wx + 1][wy - 2]] === 3 && table[wx + 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else if (chessTypes[table[wx - 1][wy + 2]] === 3 && table[wx - 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else if (chessTypes[table[wx - 1][wy - 2]] === 3 && table[wx - 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else {
					cormz = false;
				}
			}
		} else {
			if (wy === 0) {
				if (chessTypes[table[wx + 2][wy + 1]] === 3 && table[wx + 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[wx - 2][wy + 1]] === 3 && table[wx - 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else if (chessTypes[table[wx + 1][wy + 2]] === 3 && table[wx + 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else if (chessTypes[table[wx - 1][wy + 2]] === 3 && table[wx - 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else {
					cormz = false;
				}
			} else if (wy === 1) {
				if (chessTypes[table[wx + 2][wy + 1]] === 3 && table[wx + 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[wx - 2][wy + 1]] === 3 && table[wx - 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else if (chessTypes[table[wx + 1][wy + 2]] === 3 && table[wx + 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else if (chessTypes[table[wx - 1][wy + 2]] === 3 && table[wx - 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 2
				} else if (chessTypes[table[wx + 2][wy - 1]] === 3 && table[wx + 2][wy - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[wx - 2][wy - 1]] === 3 && table[wx - 2][wy - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else {
					cormz = false;
				}
			} else if (wy === 7) {
				if (chessTypes[table[wx + 2][wy - 1]] === 3 && table[wx + 2][wy - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[wx - 2][wy - 1]] === 3 && table[wx - 2][wx - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else if (chessTypes[table[wx + 1][wy - 2]] === 3 && table[wx + 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else if (chessTypes[table[wx - 1][wy - 2]] === 3 && table[wx - 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else {
					cormz = false;
				}
			} else if (wy === 6) {
				if (chessTypes[table[wx + 2][wy - 1]] === 3 && table[wx + 2][wy - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[wx - 2][wy - 1]] === 3 && table[wx - 2][wx - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else if (chessTypes[table[wx + 1][wy - 2]] === 3 && table[wx + 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else if (chessTypes[table[wx - 1][wy - 2]] === 3 && table[wx - 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else if (chessTypes[table[wx + 2][wy + 1]] === 3 && table[wx + 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[wx - 2][wy + 1]] === 3 && table[wx - 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else {
					cormz = false;
				}
			} else {
				if (chessTypes[table[wx + 2][wy - 1]] === 3 && table[wx + 2][wy - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else if (chessTypes[table[wx - 2][wy - 1]] === 3 && table[wx - 2][wx - 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[wx + 1][wy - 2]] === 3 && table[wx + 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else if (chessTypes[table[wx - 1][wy - 2]] === 3 && table[wx - 1][wy - 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else if (chessTypes[table[wx + 2][wy + 1]] === 3 && table[wx + 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[wx - 2][wy + 1]] === 3 && table[wx - 2][wy + 1] > 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else if (chessTypes[table[wx + 1][wy + 2]] === 3 && table[wx + 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else if (chessTypes[table[wx - 1][wy + 2]] === 3 && table[wx - 1][wy + 2] > 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else {
					cormz = false;
				}
			}
		}

		if (cormb || cormt || cormr || corml || cormtr || cormtl || cormbr || cormbl || cormp || cormz) {
			return true;
		} else {
			return false;
		}
	}

	if (mc === 2) {
		for (let i = lx - 1; i >= 0; i--) {  // I virsu
			if (table[i][ly] > 17 && table[i][ly] !== 0) {
				cormt = false;
				break;
			} else {
				if (chessTypes[table[i][ly]] === 2 || chessTypes[table[i][ly]] === 5) {
					cormt = true;
					cp = 1;
					break;
				} else if (table[i][ly] !== 0) {
					cormt = false;
					break;
				}
			}
		}

		for (let i = lx + 1; i <= 7; i++) {  // I apacia
			if (table[i][ly] > 17 && table[i][ly] !== 0) {
				cormb = false;
				break;
			} else {
				if (chessTypes[table[i][ly]] === 2 || chessTypes[table[i][ly]] === 5) {
					cormb = true;
					cp = 5;
					break;
				} else if (table[i][ly] !== 0) {
					cormb = false;
					break;
				}
			}
		}

		for (let i = ly + 1; i <= 7; i++) {  // I desine
			if (table[lx][i] > 17 && table[lx][i] !== 0) {
				cormr = false;
				break;
			} else {
				if (chessTypes[table[lx][i]] === 2 || chessTypes[table[lx][i]] === 5) {
					cormr = true;
					cp = 3;
					break;
				} else if (table[lx][i] !== 0) {
					cormr = false;
					break;
				}
			}
		}

		for (let i = ly - 1; i >= 0; i--) {  // I kaire
			if (table[lx][i] > 17 && table[lx][i] !== 0) {
				corml = false;
				break;
			} else {
				if (chessTypes[table[lx][i]] === 2 || chessTypes[table[lx][i]] === 5) {
					corml = true;
					cp = 7;
					break;
				} else if (table[lx][i] !== 0) {
					corml = false;
					break;
				}
			}
		}

		(7 - lx - ly) < 0 ? ptx = 7 - ly : ptx = lx;

		ly - lx < 1 ? pbx = ly : pbx = lx;

		lx - ly > 0 ? prx = 7 - lx : prx = 7 - ly;

		ly - lx > 0 ? plx = ly : plx = 7 - lx;

		for (let i = 1; i <= ptx; i++) { // I virsu desiniau
			if (table[lx - i][ly + i] > 17 && table[lx - i][ly + i] !== 0) {
				cormtr = false;
				break;
			} else {
				if (chessTypes[table[lx - i][ly + i]] === 5 || chessTypes[table[lx - i][ly + i]] === 4) {
					cormtr = true;
					cp = 2;
					break;
				} else if (table[lx - i][ly + i] !== 0) {
					cormtr = false;
					break;
				}
			}
		}

		for (let i = 1; i <= pbx; i++) { // I virsu kairiau
			if (table[lx - i][ly - i] > 17 && table[lx - i][ly - i] !== 0) {
				cormtl = false;
				break;
			} else {
				if (chessTypes[table[lx - i][ly - i]] === 5 || chessTypes[table[lx - i][ly - i]] === 4) {
					cormtl = true;
					cp = 8;
					break;
				} else if (table[lx - i][ly - i] !== 0) {
					cormtl = false;
					break;
				}
			}
		}

		for (let i = 1; i <= prx; i++) { // I apacia desiniau
			if (table[lx + i][ly + i] > 17 && table[lx + i][ly + i] !== 0) {
				cormbr = false;
				break;
			} else {
				if (chessTypes[table[lx + i][ly + i]] === 5 || chessTypes[table[lx + i][ly + i]] === 4) {
					cormbr = true;
					cp = 4;
					break;
				} else if (table[lx + i][ly + i] !== 0) {
					cormbr = false;
					break;
				}
			}
		}

		for (let i = 1; i <= plx; i++) { // I apacia kairiau
			if (table[lx + i][ly - i] > 17 && table[lx + i][ly - i] !== 0) {
				cormbl = false;
				break;
			} else {
				if (chessTypes[table[lx + i][ly - i]] === 5 || chessTypes[table[lx + i][ly - i]] === 4) {
					cormbl = true;
					cp = 6;
					break;
				} else if (table[lx + i][ly - i] !== 0) {
					cormbl = false;
					break;
				}
			}
		}

		if (lx !== 7) {  // Pestininku
			if (ly === 0) {
				if (chessTypes[table[lx + 1][ly + 1]] === 1 && table[lx + 1][ly + 1] < 18) {
					cormp = true;
				} else {
					cormp = false;
				}
			} else if (ly === 7) {
				if (chessTypes[table[lx + 1][ly - 1]] === 1 && table[lx + 1][ly - 1] < 18) {
					cormp = true;
				} else {
					cormp = false;
				}
			} else {
				if (chessTypes[table[lx + 1][ly + 1]] === 1 && table[lx + 1][ly + 1] < 18) {
					cormp = true;
				} else if (chessTypes[table[lx + 1][ly - 1]] === 1 && table[lx + 1][ly - 1] < 18) {
					cormp = true;
				} else {
					cormp = false;
				}
			}
		}

		if (lx === 7) {  // Zirgu
			if (ly === 0) {
				if (chessTypes[table[lx - 2][ly + 1]] === 3 && table[lx - 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else if (chessTypes[table[lx - 1][ly + 2]] === 3 && table[lx - 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else {
					cormz = false;
				}
			} else if (ly === 1) {
				if (chessTypes[table[lx - 2][ly + 1]] === 3 && table[lx - 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else if (chessTypes[table[lx - 1][ly + 2]] === 3 && table[lx - 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else if (chessTypes[table[lx - 2][ly - 1]] === 3 && table[lx - 2][ly - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else {
					cormz = false;
				}
			} else if (ly === 7) {
				if (chessTypes[table[lx - 2][ly - 1]] === 3 && table[lx - 2][ly - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else if (chessTypes[table[lx - 1][ly - 2]] === 3 && table[lx - 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else {
					cormz = false;
				}
			} else if (ly === 6) {
				if (chessTypes[table[lx - 2][ly - 1]] === 3 && table[lx - 2][ly - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else if (chessTypes[table[lx - 1][ly - 2]] === 3 && table[lx - 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else if (chessTypes[table[lx - 2][ly + 1]] === 3 && table[lx - 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else {
					cormz = false;
				}
			} else {
				if (chessTypes[table[lx - 2][ly + 1]] === 3 && table[lx - 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else if (chessTypes[table[lx - 2][ly - 1]] === 3 && table[lx - 2][ly - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else if (chessTypes[table[lx - 1][ly - 2]] === 3 && table[lx - 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else if (chessTypes[table[lx - 1][ly + 2]] === 3 && table[lx - 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else {
					cormz = false;
				}
			}
		} else if (lx === 6) {
			if (ly === 0) {
				if (chessTypes[table[lx - 2][ly + 1]] === 3 && table[lx - 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else if (chessTypes[table[lx - 1][ly + 2]] === 3 && table[lx - 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else if (chessTypes[table[lx + 1][ly + 2]] === 3 && table[lx + 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else {
					cormz = false;
				}
			} else if (ly === 1) {
				if (chessTypes[table[lx - 2][ly + 1]] === 3 && table[lx - 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else if (chessTypes[table[lx - 1][ly + 2]] === 3 && table[lx - 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else if (chessTypes[table[lx - 2][ly - 1]] === 3 && table[lx - 2][ly - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else if (chessTypes[table[lx + 1][ly + 2]] === 3 && table[lx + 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else {
					cormz = false;
				}
			} else if (ly === 7) {
				if (chessTypes[table[lx - 2][ly - 1]] === 3 && table[lx - 2][ly - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else if (chessTypes[table[lx - 1][ly - 2]] === 3 && table[lx - 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else if (chessTypes[table[lx + 1][ly - 2]] === 3 && table[lx + 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else {
					cormz = false;
				}
			} else if (ly === 6) {
				if (chessTypes[table[lx - 2][ly - 1]] === 3 && table[lx - 2][ly - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else if (chessTypes[table[lx - 1][ly - 2]] === 3 && table[lx - 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else if (chessTypes[table[lx - 2][ly + 1]] === 3 && table[lx - 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else if (chessTypes[table[lx + 1][ly - 2]] === 3 && table[lx + 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else {
					cormz = false;
				}
			} else {
				if (chessTypes[table[lx - 2][ly + 1]] === 3 && table[lx - 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else if (chessTypes[table[lx - 2][ly - 1]] === 3 && table[lx - 2][ly - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else if (chessTypes[table[lx - 1][ly - 2]] === 3 && table[lx - 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else if (chessTypes[table[lx - 1][ly + 2]] === 3 && table[lx - 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else if (chessTypes[table[lx + 1][ly + 2]] === 3 && table[lx + 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else if (chessTypes[table[lx + 1][ly - 2]] === 3 && table[lx + 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else {
					cormz = false;
				}
			}
		} else if (lx === 0) {
			if (ly === 0) {
				if (chessTypes[table[lx + 2][ly + 1]] === 3 && table[lx + 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[lx + 1][ly + 2]] === 3 && table[lx + 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else {
					cormz = false;
				}
			} else if (ly === 1) {
				if (chessTypes[table[lx + 2][ly + 1]] === 3 && table[lx + 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[lx + 1][ly + 2]] === 3 && table[lx + 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else if (chessTypes[table[lx + 2][ly - 1]] === 3 && table[lx + 2][ly - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else {
					cormz = false;
				}
			} else if (ly === 7) {
				if (chessTypes[table[lx + 2][ly - 1]] === 3 && table[lx + 2][ly - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[lx + 1][ly - 2]] === 3 && table[lx + 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else {
					cormz = false;
				}
			} else if (ly === 6) {
				if (chessTypes[table[lx + 2][ly - 1]] === 3 && table[lx + 2][ly - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[lx + 1][ly - 2]] === 3 && table[lx + 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else if (chessTypes[table[lx + 2][ly + 1]] === 3 && table[lx + 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else {
					cormz = false;
				}
			} else {
				if (chessTypes[table[lx + 2][ly + 1]] === 3 && table[lx + 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[lx + 2][ly - 1]] === 3 && table[lx + 2][ly - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[lx + 1][ly + 2]] === 3 && table[lx + 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else if (chessTypes[table[lx + 1][ly - 2]] === 3 && table[lx + 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else {
					cormz = false;
				}
			}
		} else if (lx === 1) {
			if (ly === 0) {
				if (chessTypes[table[lx + 2][ly + 1]] === 3 && table[lx + 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[lx + 1][ly + 2]] === 3 && table[lx + 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else if (chessTypes[table[lx - 1][ly + 2]] === 3 && table[lx - 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else {
					cormz = false;
				}
			} else if (ly === 1) {
				if (chessTypes[table[lx + 2][ly + 1]] === 3 && table[lx + 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[lx + 1][ly + 2]] === 3 && table[lx + 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 3
				} else if (chessTypes[table[lx + 2][ly - 1]] === 3 && table[lx + 2][ly - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[lx - 1][ly + 2]] === 3 && table[lx - 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else {
					cormz = false;
				}
			} else if (ly === 7) {
				if (chessTypes[table[lx + 2][ly - 1]] === 3 && table[lx + 2][ly - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[lx + 1][ly - 2]] === 3 && table[lx + 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else if (chessTypes[table[lx - 1][ly - 2]] === 3 && table[lx - 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else {
					cormz = false;
				}
			} else if (ly === 6) {
				if (chessTypes[table[lx + 2][ly - 1]] === 3 && table[lx + 2][ly - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[lx + 1][ly - 2]] === 3 && table[lx + 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else if (chessTypes[table[lx + 2][ly + 1]] === 3 && table[lx + 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[lx - 1][ly - 2]] === 3 && table[lx - 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else {
					cormz = false;
				}
			} else {
				if (chessTypes[table[lx + 2][ly + 1]] === 3 && table[lx + 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[lx + 2][ly - 1]] === 3 && table[lx + 2][ly - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[lx + 1][ly + 2]] === 3 && table[lx + 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else if (chessTypes[table[lx + 1][ly - 2]] === 3 && table[lx + 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else if (chessTypes[table[lx - 1][ly + 2]] === 3 && table[lx - 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else if (chessTypes[table[lx - 1][ly - 2]] === 3 && table[lx - 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else {
					cormz = false;
				}
			}
		} else {
			if (ly === 0) {
				if (chessTypes[table[lx + 2][ly + 1]] === 3 && table[lx + 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[lx - 2][ly + 1]] === 3 && table[lx - 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else if (chessTypes[table[lx + 1][ly + 2]] === 3 && table[lx + 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else if (chessTypes[table[lx - 1][ly + 2]] === 3 && table[lx - 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else {
					cormz = false;
				}
			} else if (ly === 1) {
				if (chessTypes[table[lx + 2][ly + 1]] === 3 && table[lx + 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[lx - 2][ly + 1]] === 3 && table[lx - 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else if (chessTypes[table[lx + 1][ly + 2]] === 3 && table[lx + 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else if (chessTypes[table[lx - 1][ly + 2]] === 3 && table[lx - 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 2
				} else if (chessTypes[table[lx + 2][ly - 1]] === 3 && table[lx + 2][ly - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[lx - 2][ly - 1]] === 3 && table[lx - 2][ly - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else {
					cormz = false;
				}
			} else if (ly === 7) {
				if (chessTypes[table[lx + 2][ly - 1]] === 3 && table[lx + 2][ly - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[lx - 2][ly - 1]] === 3 && table[lx - 2][lx - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else if (chessTypes[table[lx + 1][ly - 2]] === 3 && table[lx + 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else if (chessTypes[table[lx - 1][ly - 2]] === 3 && table[lx - 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else {
					cormz = false;
				}
			} else if (ly === 6) {
				if (chessTypes[table[lx + 2][ly - 1]] === 3 && table[lx + 2][ly - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[lx - 2][ly - 1]] === 3 && table[lx - 2][lx - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else if (chessTypes[table[lx + 1][ly - 2]] === 3 && table[lx + 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else if (chessTypes[table[lx - 1][ly - 2]] === 3 && table[lx - 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else if (chessTypes[table[lx + 2][ly + 1]] === 3 && table[lx + 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[lx - 2][ly + 1]] === 3 && table[lx - 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else {
					cormz = false;
				}
			} else {
				if (chessTypes[table[lx + 2][ly - 1]] === 3 && table[lx + 2][ly - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 8;
				} else if (chessTypes[table[lx - 2][ly - 1]] === 3 && table[lx - 2][lx - 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 5;
				} else if (chessTypes[table[lx + 1][ly - 2]] === 3 && table[lx + 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 6;
				} else if (chessTypes[table[lx - 1][ly - 2]] === 3 && table[lx - 1][ly - 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 7;
				} else if (chessTypes[table[lx + 2][ly + 1]] === 3 && table[lx + 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 4;
				} else if (chessTypes[table[lx - 2][ly + 1]] === 3 && table[lx - 2][ly + 1] < 18) {
					cormz = true;
					cp = 9;
					ur = 1;
				} else if (chessTypes[table[lx + 1][ly + 2]] === 3 && table[lx + 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 3;
				} else if (chessTypes[table[lx - 1][ly + 2]] === 3 && table[lx - 1][ly + 2] < 18) {
					cormz = true;
					cp = 9;
					ur = 2;
				} else {
					cormz = false;
				}
			}
		}

		if (cormb || cormt || cormr || corml || cormtr || cormtl || cormbr || cormbl || cormp || cormz) {
			return true;
		} else {
			return false;
		}
	}
}

function checkPos(sx, sy, cx, cy) {
	let chessColor;
	let chessType = chessTypes[table[sx][sy]];
	let chessMove;

	table[sx][sy] > 17 ? chessColor = 2 : chessColor = 1;

	table[cx][cy] === 0 ? chessMove = 2 : chessMove = 1;

	if (chessMove === 2) {  //Pestinkai eina
		if (chessType === 1) {
			if (chessColor === 1) {
				if (sx - 1 === cx && sy === cy) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
					return true;
				} else if (sx - 2 === cx && firstMove[table[sx][sy]] === 0 && sy === cy && table[sx-1][sy] === 0) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
					enPassant[table[sx][sy]] = 1;
					return true;
				} else {
					return false;
				}
			} else if (chessColor === 2) {
				if (sx + 1 === cx && sy === cy) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
					return true;
				} else if (sx + 2 === cx && firstMove[table[sx][sy]] === 0 && y === cy && table[sx+1][sy] === 0) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
					enPassant[table[sx][sy]] = 1;
					return true;
				} else {
					return false;
				}
			}
		}
	}

	if (chessMove === 1) { //Pestinkai kerta
		if (chessType === 1) {
			if (chessColor === 1) {
				if (cx === sx - 1 && cy === sy + 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
					return true;
				} else if (cx === sx - 1 && cy === sy - 1) {
					for (let i = 26; i < 18; i++) {
						enPassant[i] = 0;
					}
					return true;
				} else if (enPassant[table[cx][cy]] !== 0 && cx === sx && chessTypes[table[cx][cy]] === 1) {
					if (cy === sy + 1 || cy === sy - 1) {
						for (let i = 10; i < 18; i++) {
							enPassant[i] = 0;
						}
						enPassant[table[cx][cy]] = 1;
						return true;
					}
				} else {
					return false;
				}
			}

			if (chessColor === 2) {
				if (cx === sx + 1 && cy === sy + 1) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
					return true;
				} else if (cx === sx + 1 && cy === sy - 1) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
					return true;
				} else if (enPassant[table[cx][cy]] !== 0 && cx === sx && chessTypes[table[cx][cy]] === 1) {
					if (cy === sy + 1 || cy === sy - 1) {
						for (let i = 26; i < 34; i++) {
							enPassant[i] = 0;
						}
						enPassant[table[cx][cy]] = 1;
						return true;
					}
				} else {
					return false;
				}
			}
		}
	}

	if (chessType === 2) {  // Bokstai
		let c2d = false;

		if (cx === sx && cy < sy) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (sy - cy === 1) {
					c2d = true;
				} else {
					for (let i = 1; i < sy - cy; i++) {
						if (table[sx][sy - i] === 0) {
							c2d = true;
						} else {
							c2d = false;
							break;
						}
					}
				}
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (sy - cy === 1) {
					c2d = true;
				} else {
					for (let i = 1; i < sy - cy; i++) {
						if (table[sx][sy - i] === 0) {
							c2d = true;
						} else {
							c2d = false;
							break;
						}
					}
				}
			} else if (table[cx][cy] === 0) {
				if (sy - cy === 1) {
					c2d = true;
				} else {
					for (let i = 1; i < sy - cy; i++) {
						if (table[sx][sy - i] === 0) {
							c2d = true;
						} else {
							c2d = false;
							break;
						}
					}
				}
			}
		}

		if (cx === sx && cy > sy) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (cy - sy === 1) {
					c2d = true;
				} else {
					for (let i = 1; i < cy - sy; i++) {
						if (table[sx][sy + i] === 0) {
							c2d = true;
						} else {
							c2d = false;
							break;
						}
					}
				}
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (cy - sy === 1) {
					c2d = true;
				} else {
					for (let i = 1; i < cy - sy; i++) {
						if (table[sx][sy + i] === 0) {
							c2d = true;
						} else {
							c2d = false;
							break;
						}
					}
				}
			} else if (table[cx][cy] === 0) {
				if (cy - sy === 1) {
					c2d = true;
				} else {
					for (let i = 1; i < cy - sy; i++) {
						if (table[sx][sy + i] === 0) {
							c2d = true;
						} else {
							c2d = false;
							break;
						}
					}
				}
			}
		}

		if (cy === sy && cx > sx) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (cx - sx === 1) {
					c2d = true;
				} else {
					for (let i = 1; i < cx - x; i++) {
						if (table[sx + i][sy] === 0) {
							c2d = true;
						} else {
							c2d = false;
							break;
						}
					}
				}
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (cx - sx === 1) {
					c2d = true;
				} else {
					for (let i = 1; i < cx - x; i++) {
						if (table[sx + i][sy] === 0) {
							c2d = true;
						} else {
							c2d = false;
							break;
						}
					}
				}
			} else if (table[cx][cy] === 0) {
				if (cx - sx === 1) {
					c2d = true;
				} else {
					for (let i = 1; i < cx - x; i++) {
						if (table[sx + i][sy] === 0) {
							c2d = true;
						} else {
							c2d = false;
							break;
						}
					}
				}
			}
		}

		if (cy === sy && cx < sx) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (sx - cx === 1) {
					c2d = true;
				} else {
					for (let i = 1; i < sx - cx; i++) {
						if (table[sx - i][sy] === 0) {
							c2d = true;
						} else {
							c2d = false;
							break;
						}
					}
				}
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (sx - cx === 1) {
					c2d = true;
				} else {
					for (let i = 1; i < sx - cx; i++) {
						if (table[sx - i][sy] === 0) {
							c2d = true;
						} else {
							c2d = false;
							break;
						}
					}
				}
			} else if (table[cx][cy] === 0) {
				if (sx - cx === 1) {
					c2d = true;
				} else {
					for (let i = 1; i < sx - cx; i++) {
						if (table[sx - i][sy] === 0) {
							c2d = true;
						} else {
							c2d = false;
							break;
						}
					}
				}
			}
		}

		if (chessColor === 1) {
			for (let i = 10; i < 18; i++) {
				enPassant[i] = 0;
			}
		}
		if (chessColor === 2) {
			for (let i = 26; i < 34; i++) {
				enPassant[i] = 0;
			}
		}
		return c2d;
	}

	if (chessType === 3) {  //Zirgai
		let c3d = false;

		if (cx === sx + 2 && cy === sy + 1) {
			c3d = true;
		}

		if (cx === sx + 2 && cy === sy - 1) {
			c3d = true;
		}

		if (cx === sx - 2 && cy === sy + 1) {
			c3d = true;
		}

		if (cx === sx - 2 && cy === sy - 1) {
			c3d = true;
		}

		if (cx === sx + 1 && cy === sy + 2) {
			c3d = true;
		}

		if (cx === sx - 1 && cy === sy + 2) {
			c3d = true;
		}

		if (cx === sx + 1 && cy === sy - 2) {
			c3d = true;
		}

		if (cx === sx - 1 && cy === sy - 2) {
			c3d = true;
		}

		if (chessColor === 1) {
			for (let i = 10; i < 18; i++) {
				enPassant[i] = 0;
			}
		}
		if (chessColor === 2) {
			for (let i = 26; i < 34; i++) {
				enPassant[i] = 0;
			}
		}
		return c3d;
	}

	if (chessType === 4) {  //Rikiai
		let c4d = false;

		if (cx > sx && cy > sy) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (cx - sx === cy - sy) {
					if (cx - sx === 1 && cy - sy === 1) {
						c4d = true;
					} else {
						for (let i = 1; i < cx - sx; i++) {
							if (table[sx + i][sy + i] === 0) {
								c4d = true;
							} else {
								c4d = false;
								break;
							}
						}
					}
				}
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (cx - sx === cy - sy) {
					if (cx - sx === 1 && cy - sy === 1) {
						c4d = true;
					} else {
						for (let i = 1; i < cx - sx; i++) {
							if (table[sx + i][sy + i] === 0) {
								c4d = true;
							} else {
								c4d = false;
								break;
							}
						}
					}
				}
			} else if (table[cx][cy] === 0) {
				if (cx - sx === cy - sy) {
					if (cx - sx === 1 && cy - sy === 1) {
						c4d = true;
					} else {
						for (let i = 1; i < cx - sx; i++) {
							if (table[sx + i][sy + i] === 0) {
								c4d = true;
							} else {
								c4d = false;
								break;
							}
						}
					}
				}
			}
		}

		if (cx > sx && cy < sy) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (cx - sx === sy - cy) {
					if (cx - sx === 1 && sy - cy === 1) {
						c4d = true;
					} else {
						for (let i = 1; i < cx - sx; i++) {
							if (table[sx + i][sy - i] === 0) {
								c4d = true;
							} else {
								c4d = false;
								break;
							}
						}
					}
				}
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (cx - sx === sy - cy) {
					if (cx - sx === 1 && sy - cy === 1) {
						c4d = true;
					} else {
						for (let i = 1; i < cx - sx; i++) {
							if (table[sx + i][sy - i] === 0) {
								c4d = true;
							} else {
								c4d = false;
								break;
							}
						}
					}
				}
			} else if (table[cx][cy] === 0) {
				if (cx - sx === sy - cy) {
					if (cx - sx === 1 && sy - cy === 1) {
						c4d = true;
					} else {
						for (let i = 1; i < cx - sx; i++) {
							if (table[sx + i][sy - i] === 0) {
								c4d = true;
							} else {
								c4d = false;
								break;
							}
						}
					}
				}
			}
		}

		if (cx < sx && cy > sy) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (sx - cx === cy - sy) {
					if (sx - cx === 1 && cy - sy === 1) {
						c4d = true;
					} else {
						for (let i = 1; i < cy - sy; i++) {
							if (table[sx - i][sy + i] === 0) {
								c4d = true;
							} else {
								c4d = false;
								break;
							}
						}
					}
				}
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (sx - cx === cy - sy) {
					if (sx - cx === 1 && cy - sy === 1) {
						c4d = true;
					} else {
						for (let i = 1; i < cy - sy; i++) {
							if (table[sx - i][sy + i] === 0) {
								c4d = true;
							} else {
								c4d = false;
								break;
							}
						}
					}
				}
			} else if (table[cx][cy] === 0) {
				if (sx - cx === cy - sy) {
					if (sx - cx === 1 && cy - sy === 1) {
						c4d = true;
					} else {
						for (let i = 1; i < cy - sy; i++) {
							if (table[sx - i][sy + i] === 0) {
								c4d = true;
							} else {
								c4d = false;
								break;
							}
						}
					}
				}
			}
		}

		if (cx < sx && cy < sy) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (sx - cx === sy - cy) {
					if (sx - cx === 1 && sy - cy === 1) {
						c4d = true;
					} else {
						for (let i = 1; i < sy - cy; i++) {
							if (table[sx - i][sy - i] === 0) {
								c4d = true;
							} else {
								c4d = false;
								break;
							}
						}
					}
				}
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (sx - cx === sy - cy) {
					if (sx - cx === 1 && sy - cy === 1) {
						c4d = true;
					} else {
						for (let i = 1; i < sy - cy; i++) {
							if (table[sx - i][sy - i] === 0) {
								c4d = true;
							} else {
								c4d = false;
								break;
							}
						}
					}
				}
			} else if (table[cx][cy] === 0) {
				if (sx - cx === sy - cy) {
					if (sx - cx === 1 && sy - cy === 1) {
						c4d = true;
					} else {
						for (let i = 1; i < sy - cy; i++) {
							if (table[sx - i][sy - i] === 0) {
								c4d = true;
							} else {
								c4d = false;
								break;
							}
						}
					}
				}
			}
		}

		if (chessColor === 1) {
			for (let i = 10; i < 18; i++) {
				enPassant[i] = 0;
			}
		}
		if (chessColor === 2) {
			for (let i = 26; i < 34; i++) {
				enPassant[i] = 0;
			}
		}
		return c4d;
	}

	if (chessType === 5) {  //Karalienes
		let c5d = false;

		if (cx === sx && cy < sy) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (sy - cy === 1) {
					c5d = true;
				} else {
					for (let i = 1; i < sy - cy; i++) {
						if (table[sx][sy - i] === 0) {
							c5d = true;
						} else {
							c5d = false;
							break;
						}
					}
				}
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (sy - cy === 1) {
					c5d = true;
				} else {
					for (let i = 1; i < sy - cy; i++) {
						if (table[sx][sy - i] === 0) {
							c5d = true;
						} else {
							c5d = false;
							break;
						}
					}
				}
			} else if (table[cx][cy] === 0) {
				if (sy - cy === 1) {
					c5d = true;
				} else {
					for (let i = 1; i < sy - cy; i++) {
						if (table[sx][sy - i] === 0) {
							c5d = true;
						} else {
							c5d = false;
							break;
						}
					}
				}
			}
		}

		if (cx === sx && cy > sy) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (cy - sy === 1) {
					c5d = true;
				} else {
					for (let i = 1; i < cy - sy; i++) {
						if (table[sx][sy + i] === 0) {
							c5d = true;
						} else {
							c5d = false;
							break;
						}
					}
				}
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (cy - sy === 1) {
					c5d = true;
				} else {
					for (let i = 1; i < cy - sy; i++) {
						if (table[sx][sy + i] === 0) {
							c5d = true;
						} else {
							c5d = false;
							break;
						}
					}
				}
			} else if (table[cx][cy] === 0) {
				if (cy - sy === 1) {
					c5d = true;
				} else {
					for (let i = 1; i < cy - sy; i++) {
						if (table[sx][sy + i] === 0) {
							c5d = true;
						} else {
							c5d = false;
							break;
						}
					}
				}
			}
		}

		if (cy === sy && cx > sx) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (cx - sx === 1) {
					c5d = true;
				} else {
					for (let i = 1; i < cx - sx; i++) {
						if (table[sx + i][sy] === 0) {
							c5d = true;
						} else {
							c5d = false;
							break;
						}
					}
				}
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (cx - sx === 1) {
					c5d = true;
				} else {
					for (let i = 1; i < cx - sx; i++) {
						if (table[sx + i][sy] === 0) {
							c5d = true;
						} else {
							c5d = false;
							break;
						}
					}
				}
			} else if (table[cx][cy] === 0) {
				if (cx - sx === 1) {
					c5d = true;
				} else {
					for (let i = 1; i < cx - sx; i++) {
						if (table[sx + i][sy] === 0) {
							c5d = true;
						} else {
							c5d = false;
							break;
						}
					}
				}
			}
		}

		if (cy === sy && cx < sx) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (sx - cx === 1) {
					c5d = true;
				} else {
					for (let i = 1; i < sx - cx; i++) {
						if (table[sx - i][sy] === 0) {
							c5d = true;
						} else {
							c5d = false;
							break;
						}
					}
				}
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (sx - cx === 1) {
					c5d = true;
				} else {
					for (let i = 1; i < sx - cx; i++) {
						if (table[sx - i][sy] === 0) {
							c5d = true;
						} else {
							c5d = false;
							break;
						}
					}
				}
			} else if (table[cx][cy] === 0) {
				if (sx - cx === 1) {
					c5d = true;
				} else {
					for (let i = 1; i < sx - cx; i++) {
						if (table[sx - i][sy] === 0) {
							c5d = true;
						} else {
							c5d = false;
							break;
						}
					}
				}
			}
		}

		if (cx > sx && cy > sy) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (cx - sx === cy - sy) {
					if (cx - sx === 1 && cy - sy === 1) {
						c5d = true;
					} else {
						for (let i = 1; i < cx - sx; i++) {
							if (table[sx + i][sy + i] === 0) {
								c5d = true;
							} else {
								c5d = false;
								break;
							}
						}
					}
				}
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (cx - sx === cy - sy) {
					if (cx - sx === 1 && cy - sy === 1) {
						c5d = true;
					} else {
						for (let i = 1; i < cx - sx; i++) {
							if (table[sx + i][sy + i] === 0) {
								c5d = true;
							} else {
								c5d = false;
								break;
							}
						}
					}
				}
			} else if (table[cx][cy] === 0) {
				if (cx - sx === cy - sy) {
					if (cx - sx === 1 && cy - sy === 1) {
						c5d = true;
					} else {
						for (let i = 1; i < cx - sx; i++) {
							if (table[sx + i][sy + i] === 0) {
								c5d = true;
							} else {
								c5d = false;
								break;
							}
						}
					}
				}
			}
		}

		if (cx > sx && cy < sy) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (cx - sx === sy - cy) {
					if (cx - sx === 1 && sy - cy === 1) {
						c5d = true;
					} else {
						for (let i = 1; i < cx - sx; i++) {
							if (table[sx + i][sy - i] === 0) {
								c5d = true;
							} else {
								c5d = false;
								break;
							}
						}
					}
				}
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (cx - sx === sy - cy) {
					if (cx - sx === 1 && sy - cy === 1) {
						c5d = true;
					} else {
						for (let i = 1; i < cx - sx; i++) {
							if (table[sx + i][sy - i] === 0) {
								c5d = true;
							} else {
								c5d = false;
								break;
							}
						}
					}
				}
			} else if (table[cx][cy] === 0) {
				if (cx - sx === sy - cy) {
					if (cx - sx === 1 && sy - cy === 1) {
						c5d = true;
					} else {
						for (let i = 1; i < cx - sx; i++) {
							if (table[sx + i][sy - i] === 0) {
								c5d = true;
							} else {
								c5d = false;
								break;
							}
						}
					}
				}
			}
		}

		if (cx < sx && cy > sy) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (sx - cx === cy - sy) {
					if (sx - cx === 1 && cy - sy === 1) {
						c5d = true;
					} else {
						for (let i = 1; i < cy - sy; i++) {
							if (table[sx - i][sy + i] === 0) {
								c5d = true;
							} else {
								c5d = false;
								break;
							}
						}
					}
				}
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (sx - cx === cy - sy) {
					if (sx - cx === 1 && cy - sy === 1) {
						c5d = true;
					} else {
						for (let i = 1; i < cy - sy; i++) {
							if (table[sx - i][sy + i] === 0) {
								c5d = true;
							} else {
								c5d = false;
								break;
							}
						}
					}
				}
			} else if (table[cx][cy] === 0) {
				if (sx - cx === cy - sy) {
					if (sx - cx === 1 && cy - sy === 1) {
						c5d = true;
					} else {
						for (let i = 1; i < cy - sy; i++) {
							if (table[sx - i][sy + i] === 0) {
								c5d = true;
							} else {
								c5d = false;
								break;
							}
						}
					}
				}
			}
		}

		if (cx < sx && cy < sy) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (sx - cx === sy - cy) {
					if (sx - cx === 1 && sy - cy === 1) {
						c5d = true;
					} else {
						for (let i = 1; i < sy - cy; i++) {
							if (table[sx - i][sy - i] === 0) {
								c5d = true;
							} else {
								c5d = false;
								break;
							}
						}
					}
				}
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (sx - cx === sy - cy) {
					if (sx - cx === 1 && sy - cy === 1) {
						c5d = true;
					} else {
						for (let i = 1; i < sy - cy; i++) {
							if (table[sx - i][sy - i] === 0) {
								c5d = true;
							} else {
								c5d = false;
								break;
							}
						}
					}
				}
			} else if (table[cx][cy] === 0) {
				if (sx - cx === sy - cy) {
					if (sx - cx === 1 && sy - cy === 1) {
						c5d = true;
					} else {
						for (let i = 1; i < sy - cy; i++) {
							if (table[sx - i][sy - i] === 0) {
								c5d = true;
							} else {
								c5d = false;
								break;
							}
						}
					}
				}
			}
		}

		if (chessColor === 1) {
			for (let i = 10; i < 18; i++) {
				enPassant[i] = 0;
			}
		}
		if (chessColor === 2) {
			for (let i = 26; i < 34; i++) {
				enPassant[i] = 0;
			}
		}
		return c5d;
	}

	if (chessType === 6) {  //Karaliai
		if (cx === sx - 1 && cy === sy && checkKing(cx, cy, chessColor)) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			} else if (table[cx][cy] === 0) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			}
		}

		if (cx === sx - 1 && cy === sy + 1 && checkKing(cx, cy, chessColor)) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			} else if (table[cx][cy] === 0) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			}
		}

		if (cx === sx && cy === sy + 1 && checkKing(cx, cy, chessColor)) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			} else if (table[cx][cy] === 0) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			}
		}

		if (cx === sx + 1 && cy === sy + 1 && checkKing(cx, cy, chessColor)) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			} else if (table[cx][cy] === 0) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			}
		}

		if (cx === sx + 1 && cy === sy && checkKing(cx, cy, chessColor)) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			} else if (table[cx][cy] === 0) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			}
		}

		if (cx === sx + 1 && cy === sy - 1 && checkKing(cx, cy, chessColor)) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			} else if (table[cx][cy] === 0) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			}
		}

		if (cx === sx && cy === sy - 1 && checkKing(cx, cy, chessColor)) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			} else if (table[cx][cy] === 0) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			}
		}

		if (cx === sx - 1 && cy === sy - 1 && checkKing(cx, cy, chessColor)) {
			if (chessColor === 1 && table[cx][cy] > 17) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			} else if (chessColor === 2 && table[cx][cy] < 18) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			} else if (table[cx][cy] === 0) {
				if (chessColor === 1) {
					for (let i = 10; i < 18; i++) {
						enPassant[i] = 0;
					}
				}
				if (chessColor === 2) {
					for (let i = 26; i < 34; i++) {
						enPassant[i] = 0;
					}
				}
				return true;
			}
		}

		//Rokiruotes
		let cr = false;

		if (cx === sx && cy === sy + 2) {
			(table[sx][sy + 1] === 0 && firstMove[table[sx][sy]] === 0) ? cr = true : cr = false;

			if (cr) {
				if (chessColor === 1 && firstMove[8] === 0) {
					if (!checkMate(chessColor) && !checkChack(sx, sy, sx, sy + 1, 1)) {
						table[7][7] = 0;
						table[7][5] = 8;
						if (chessColor === 1) {
							for (let i = 10; i < 18; i++) {
								enPassant[i] = 0;
							}
						}
						if (chessColor === 2) {
							for (let i = 26; i < 34; i++) {
								enPassant[i] = 0;
							}
						}
						return true;
					} else {
						return false;
					}
				}

				if (chessColor === 2 && firstMove[25] === 0) {
					if (!checkMate(chessColor) && !checkChack(sx, sy, sx, sy + 1, 1)) {
						table[0][7] = 0;
						table[0][5] = 25;
						if (chessColor === 1) {
							for (let i = 10; i < 18; i++) {
								enPassant[i] = 0;
							}
						}
						if (chessColor === 2) {
							for (let i = 26; i < 34; i++) {
								enPassant[i] = 0;
							}
						}
						return true;
					} else {
						return false;
					}
				}
			}
		}

		if (cx === sx && cy === sy - 3) {
			(table[sx][sy - 1] === 0 && table[sx][sy - 2] === 0 && firstMove[table[sx][sy]] === 0) ? cr = true : cr = false;

			if (cr) {
				if (chessColor === 1 && firstMove[1] === 0) {
					if (!checkMate(chessColor) && !checkChack(sx, sy, sx, sy - 1, 1) && !checkChack(sx, sy, sx, sy - 2, 1)) {
						table[7][0] = 0;
						table[7][2] = 1;
						if (chessColor === 1) {
							for (let i = 10; i < 18; i++) {
								enPassant[i] = 0;
							}
						}
						if (chessColor === 2) {
							for (let i = 26; i < 34; i++) {
								enPassant[i] = 0;
							}
						}
						return true;
					} else {
						return false;
					}
				}

				if (chessColor === 2 && firstMove[18] === 0) {
					if (!checkMate(chessColor) && !checkChack(sx, sy, sx, sy - 1, 1) && !checkChack(sx, sy, sx, sy - 2, 1)) {
						table[0][0] = 0;
						table[0][2] = 18;
						if (chessColor === 1) {
							for (let i = 10; i < 18; i++) {
								enPassant[i] = 0;
							}
						}
						if (chessColor === 2) {
							for (let i = 26; i < 34; i++) {
								enPassant[i] = 0;
							}
						}
						return true;
					} else {
						return false;
					}
				}
			}
		}

		return false;
	}
}

function checkKing (rx, ry, mcc) {
	let mf;

	mcc === 1 ? mf = 22 : mf = 5;

	if (oor === 0) {
		if (table[rx - 1][ry] === mf) {
			return false;
		} else if (table[rx - 1][ry + 1] === mf) {
			return false;
		} else if (table[rx][ry + 1] === mf) {
			return false;
		} else if (table[rx + 1][ry + 1] === mf) {
			return false;
		} else if (table[rx + 1][ry] === mf) {
			return false;
		} else if (table[rx + 1][ry - 1] === mf) {
			return false;
		} else if (table[rx][ry - 1] === mf) {
			return false;
		} else if (table[rx - 1][ry - 1] === mf) {
			return false;
		} else {
			return true;
		}
	}
}